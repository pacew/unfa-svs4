Track 2: GNU/Metal

Excuse me, could I ... uuuh ... take a look at the source code, please?

GNU!

General Public License

Fork my code!

Fork me, now!
Fork my code, now!
Fork, fork my code, now!

For more lyrics, please refer to https://www.gnu.org/licenses/gpl-3.0.html.en ,
 preamble

Track 3: Open Secrets

What's your secret?

Hey, what's your secret?
Tell me, tell me!
Hey, what's your secret?
Tell me, I wanna know!

    I don't have a secret!
    I'd tell you, I'd tell you!
    I don't have no secret
    I'll tell you, I'll tell you how!

Hey, what's your secret?
Tell me, tell me!
Hey, what's your secret?
Tell me, I need to know!

    It's an open secret!
    I'll tell you, I'll tell you!
    It's an open secret!
    For everyone to know!


I don't care
how much you see of my life

my life is an open book for you

and I'll let you read me
cover to cover
all day long


Nothing to hide
I have given up the fight
against big brother

I don't get paid for training his AI
visit my channel watch me cry
