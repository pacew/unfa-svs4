stage=$(HOME)/unfa-svs4-stage

zip_file=unfa-svs4.zip

all: do-stage $(stage)/$(zip_file)

$(stage)/$(zip_file): $(stage)/track1.mp3 \
		$(stage)/track2.mp3 \
		$(stage)/track3.mp3 \
		$(stage)/track4.mp3 \
		$(stage)/track2-vid.mp4 \
		$(stage)/credits.txt \
		$(stage)/lyrics.txt \
		$(stage)/unfa-svs4-cover.png
	rm -f $(stage)/$(zip_file)
	cd $(stage) && zip $(zip_file) \
		track1.mp3 \
		track2.mp3 \
		track3.mp3 \
		track4.mp3 \
		track2-vid.mp4 \
		credits.txt \
		lyrics.txt \
		unfa-svs4-cover.png


.PHONY: do-stage
do-stage:
	./stage-files

clean:
	rm -f $(stage)/$(zip_file) $(stage)/track*.mp3 \
		$(stage)/credits.txt $(stage)/lyrics.txt \
		$(stage)/unfa-svs4-cover.png

$(stage)/track1.mp3: $(stage)/track1.flac
	ffmpeg -i $(stage)/track1.flac -c:a libmp3lame -b:a 320k $(stage)/track1.mp3

$(stage)/track2.mp3: $(stage)/track2.ogg
	ffmpeg -i $(stage)/track2.ogg -c:a libmp3lame -b:a 320k $(stage)/track2.mp3

$(stage)/track3.mp3: $(stage)/track3.flac
	ffmpeg -i $(stage)/track3.flac -c:a libmp3lame -b:a 320k $(stage)/track3.mp3

# -q:a 0 flag to preserve high frequency stuff on this track
$(stage)/track4.mp3: $(stage)/track4.flac
	ffmpeg -i $(stage)/track4.flac -c:a libmp3lame -b:a 320k -q:a 0 $(stage)/track4.mp3

get:
	rsync -azv music2:sync/unfa-svs/unfa-svs4/. .

put:
	rsync -avz `git ls-files` music2:sync/unfa-svs/unfa-svs4/.

put-zip:
	rsync -avz $(stage)/$(zip_file) music2:sync/unfa-svs/extra/unfa-svs4-draft.zip
